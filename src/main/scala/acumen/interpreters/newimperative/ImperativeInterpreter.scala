package acumen
package interpreters
package newimperative

import scala.collection.immutable.HashMap

import acumen.Errors._
import acumen.Pretty._
import acumen.util.Conversions._
import acumen.util.Random
import acumen.interpreters.Common.{ classDef, evalOp, initStoreImpr, magicClass, deviceClass }
import acumen.util.Canonical.{
  childrenOf,
  classf,
  cmain,
  cmagic,
  endTime,
  nextChild,
  parentOf,
  parent,
  seedOf,
  seed1,
  seed2,
  self,
  resultType,
  time,
  timeStep
}
import scala.annotation.tailrec

class ImperativeInterpreter(val parDiscr: Boolean = true, 
                            val parCont: Boolean = false,
                            val contWithDiscr: Boolean = false) extends CStoreInterpreter {
  override def id = Array("newimperative", 
                          if (parDiscr) "parDiscr" else "seqDiscr",
                          if (parCont) "parCont" else "seqDiscr",
                          if (contWithDiscr) "contWithDiscr" else "contWithCont")

  import Common._

  type Store = Common.Store
  def repr (s:Store) : CStore = Common.repr(s)
  def fromCStore (cs:CStore, root:CId) : Store = Common.fromCStore(cs, root)

  def init(prog: Prog): (Prog, Store) = {
    val magic = fromCStore(initStoreImpr, CId(0))
    val (sd1, sd2) = Random.split(Random.mkGen(0))
    val mainObj = mkObj(cmain, prog, IsMain, sd1, List(VObjId(Some(magic))), magic, 1)
    magic.seed = sd2
    val cprog = CleanParameters.run(prog, CStoreInterpreterType)
    val sprog = Simplifier.run(cprog)
    val mprog = Prog(magicClass :: deviceClass :: sprog.defs)
    (mprog , mainObj)
  }

  def localStep(p: Prog, st: Store): ResultType = {
    val magic = getSimulator(st)
    stepInit
    if (getTime(magic) > getEndTime(magic)) {
      null
    } else {
      val pp = magic.phaseParms
      pp.curIter += 1
      if (getResultType(magic) != FixedPoint) {
        if (parDiscr) pp.delayUpdate = true
        else          pp.delayUpdate = false
        pp.doDiscrete = true
        if (contWithDiscr) pp.doEquationT = true
        else               pp.doEquationT = false
        pp.doEquationI = false
      } else {
        if (parCont) pp.delayUpdate = true
        else         pp.delayUpdate = false
        pp.doDiscrete = false
        if (contWithDiscr) pp.doEquationT = false
        else               pp.doEquationT = true
        pp.doEquationI = true
      }

      val chtset = traverse(evalStep(p, magic), st)
      val rt = getResultType(magic) match {
        case Discrete | Continuous =>
          chtset match {
            case SomeChange(dead, rps) =>
              for ((o, p) <- rps)
                changeParent(o, p)
              for (o <- dead) {
                o.parent match {
                  case None => ()
                  case Some(op) =>
                    for (oc <- o.children) changeParent(oc, op)
                    op.children = op.children diff Seq(o)
                }
              }
              Discrete
            case NoChange() =>
              FixedPoint
          }
        case FixedPoint =>
          setTime(magic, getTime(magic) + getTimeStep(magic))
          Continuous
      }
      setResultType(magic, rt)
      rt
    }
  }

  /*def getDeviceData(deviceID: CId): Array[String] = {
    val deviceNo = deviceID.id.head
    val deviceData = BuildHost.BuildHost.sensor.get(deviceNo)
    return deviceData
  }

  def createDeviceObject(parent: Object): Object =
    Object((parent.ccounter + 1) :: parent.id
      , MMap.empty ++
        MMap((Name("ax", 0) -> new ValVal(VLit(GDouble(5.0))))
            ,(Name("ay", 0) -> new ValVal(VLit(GDouble(5.0))))
            ,(Name("az", 0) -> new ValVal(VLit(GDouble(5.0))))
            ,(Name("alpha", 0) -> new ValVal(VLit(GDouble(5.0))))
            ,(Name("beta", 0) -> new ValVal(VLit(GDouble(5.0))))
            ,(Name("gamma", 0) -> new ValVal(VLit(GDouble(5.0))))
            ,(Name("className", 0) -> new ValVal(VClassName(ClassName("Device")))))
      , new PhaseParms
      , Some(parent)
      , 1
      , (0,0)
      , Vector.empty
    )

  def addDeviceToSimulator(magic: Object, device: Object): Unit = {
    magic.children = Vector(device) ++ magic.children
    magic.ccounter += 1
    val devices = magic.fields(Name("device",0))
    magic.fields(Name("device",0)) = devices match {
      case VVector(vs) => (vs :+ VObjId(Some(device)))
      case _ => (List(VObjId(Some(device))))
    }
  }

  def addDeviceData(magic :Object) = {
    val deviceNum = BuildHost.BuildHost.sensor.size() - 1
    var devicesNow = magic.children.size
    // inject device objects and make sure the object is only created once
    if (devicesNow == 0 && deviceNum > 0){
      val deviceObject = createDeviceObject(magic)
      // add device object to simulator
      addDeviceToSimulator(magic, deviceObject)
    }
    devicesNow = magic.children.size
    if (devicesNow < deviceNum){
      for (i <- 1 to deviceNum - devicesNow){
        val deviceObject = createDeviceObject(magic)
        // add device object to simulator
        addDeviceToSimulator(magic, deviceObject)
      }
    }
    // setField should be done continuous during the process
    if (devicesNow > 0){
      for (i <- 0 to devicesNow - 1) {
        val sensorValue = getDeviceData(magic.children(i).cid)
        setField(magic.children(i), Name("ax",0), VLit(GDouble(sensorValue(0).toDouble)))
        setField(magic.children(i), Name("ay",0), VLit(GDouble(sensorValue(1).toDouble)))
        setField(magic.children(i), Name("az",0), VLit(GDouble(sensorValue(2).toDouble)))
        setField(magic.children(i), Name("alpha",0), VLit(GDouble(sensorValue(3).toDouble)))
        setField(magic.children(i), Name("beta",0), VLit(GDouble(sensorValue(4).toDouble)))
        setField(magic.children(i), Name("gamma",0), VLit(GDouble(sensorValue(5).toDouble)))
      }
    }
  }*/

  def step(p: Prog, st: Store): Option[Store] = {
    val res = localStep(p, st)
    if (res == null) None
    else Some(st)
  }

  // always returns the last known step, the adder callback is used to
  // determine when teh simulation is done
  override def multiStep(p: Prog, st: Store, adder: DataAdder): Store = {
    val magic = getSimulator(st)
    var shouldAddData = ShouldAddData.IfLast
    // ^^ set to IfLast on purpose to make things work
    @tailrec def step0() : Unit = {
      val res = localStep(p, st)
      stepInit
      if (res == null) {
        if (shouldAddData == ShouldAddData.IfLast)
          addData(st, adder)
        adder.noMoreData()
      } else {
        shouldAddData = adder.newStep(res)
        if (shouldAddData == ShouldAddData.Yes)
          addData(st, adder)
        if (adder.continue)
          step0()
      }
    }
    step0()
    getThreeDData(st)
    st
  }

  def addData(st: Store, adder: DataAdder) : Unit = {
    // Note: Conversion to a CStore just to add the data is certainly
    // not the most efficient way to go about things, but for now it
    // will do. --kevina
    adder.addData(st.id, st.fieldsCur)
    st.children.foreach { child => addData(child, adder) }
  }

  def stepInit : Unit = {}
  def traverse(f: ObjId => Changeset, root: ObjId): Changeset =
    traverseSimple(f, root)

}

object ImperativeInterpreter extends ImperativeInterpreter(true,false,false)

