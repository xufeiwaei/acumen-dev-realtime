package acumen
package interpreters
package imperative

import scala.collection.immutable.HashMap

import acumen.Errors._
import acumen.Pretty._
import acumen.util.Conversions._
import acumen.util.Random
import acumen.interpreters.Common.{ classDef, evalOp, initStoreImpr, magicClass, deviceClass }
import acumen.util.Canonical.{
childrenOf,
classf,
cmain,
cmagic,
endTime,
nextChild,
parentOf,
parent,
seedOf,
seed1,
seed2,
self,
resultType,
time,
timeStep
}
import scala.annotation.tailrec


class ImperativeInterpreter extends CStoreInterpreter {

  override def id = Array("imperative")

  import Common._

  type Store = Common.Store
  def repr (s:Store) : CStore = Common.repr(s)
  def fromCStore (cs:CStore, root:CId) : Store = Common.fromCStore(cs, root)

  def init(prog: Prog): (Prog, Store) = {
    val magic = fromCStore(initStoreImpr, CId(0))
    /* WARNING: the following line works because there is no children access check
       if one of the instructions of the provate section tries to access magic,
       and there was a check, this would crash (which we don't want) */
    val (sd1, sd2) = Random.split(Random.mkGen(0))
    val mainObj = mkObj(cmain, prog, None, sd1, List(VObjId(Some(magic))), magic, 1)
    magic.seed = sd2
    changeParent(magic, mainObj)
    val cprog = CleanParameters.run(prog, CStoreInterpreterType)
    val sprog = Simplifier.run(cprog)
    val mprog = Prog(magicClass :: deviceClass :: sprog.defs)
    (mprog , mainObj)
  }


  def localStep(p: Prog, st: Store): ResultType = {
    val magic = getSimulator(st)
    stepInit
    if (getTime(magic) > getEndTime(magic)) {
      null
    } else {
      addDeviceData(magic)
      val chtset = traverse(evalStep(p, magic), st)
      val rt = getResultType(magic) match {
        case Discrete | Continuous =>
          chtset match {
            case SomeChange(dead, rps) =>
              for ((o, p) <- rps)
                changeParent(o, p)
              for (o <- dead) {
                o.parent match {
                  case None => ()
                  case Some(op) =>
                    for (oc <- o.children) changeParent(oc, op)
                    op.children = op.children diff Seq(o)
                }
              }
              Discrete
            case NoChange() =>
              FixedPoint
          }
        case FixedPoint =>
          setTime(magic, getTime(magic) + getTimeStep(magic))
          Continuous
      }
      setResultType(magic, rt)
      rt
    }
  }

  def createDeviceObject(parent: Object): Object =
    Object((parent.ccounter + 1) :: parent.id
      , MMap.empty ++
        MMap((Name("ax", 0) -> VLit(GDouble(5.0)))
            ,(Name("ay", 0) -> VLit(GDouble(5.0)))
            ,(Name("az", 0) -> VLit(GDouble(5.0)))
            ,(Name("alpha", 0) -> VLit(GDouble(5.0)))
            ,(Name("beta", 0) -> VLit(GDouble(5.0)))
            ,(Name("gamma", 0) -> VLit(GDouble(5.0)))
            ,(Name("compassheading", 0) -> VLit(GDouble(5.0)))
            ,(Name("className", 0) -> VClassName(ClassName("Device"))))
      , Some(parent)
      , 1
      , (0,0)
      , Vector.empty
    )

  def addDeviceToSimulator(magic: Object, device: Object): Unit = {
    magic.children = Vector(device) ++ magic.children
    magic.ccounter += 1
    val devices = magic.fields(Name("device",0))
    magic.fields(Name("device",0)) = devices match {
      case VVector(vs) => VVector(vs :+ VObjId(Some(device)))
      case _ => VVector(List(VObjId(Some(device))))
    }
  }

  def addDeviceData(magic :Object) = {
    val deviceNum = BuildHost.BuildHost.sensors.size() - 1
    var devicesNow = magic.children.size
    // inject device objects and make sure the object is only created once
    if (devicesNow == 0 && deviceNum > 0){
      val deviceObject = createDeviceObject(magic)
      // add device object to simulator
      addDeviceToSimulator(magic, deviceObject)
    }
    devicesNow = magic.children.size
    if (devicesNow < deviceNum){
      for (i <- 1 to deviceNum - devicesNow){
        val deviceObject = createDeviceObject(magic)
        // add device object to simulator
        addDeviceToSimulator(magic, deviceObject)
      }
    }
    // setField should be done continuous during the process
    if (devicesNow > 0) {
      for (i <- 0 to devicesNow - 1) {
        val sensorValue = getDeviceData(magic.children(i).cid)
        setField(magic.children(i), Name("ax",0), VLit(GDouble(sensorValue(0).toDouble)))
        setField(magic.children(i), Name("ay",0), VLit(GDouble(sensorValue(1).toDouble)))
        setField(magic.children(i), Name("az",0), VLit(GDouble(sensorValue(2).toDouble)))
        setField(magic.children(i), Name("alpha",0), VLit(GDouble(sensorValue(3).toDouble)))
        setField(magic.children(i), Name("beta",0), VLit(GDouble(sensorValue(4).toDouble)))
        setField(magic.children(i), Name("gamma",0), VLit(GDouble(sensorValue(5).toDouble)))
        setField(magic.children(i), Name("compassheading",0), VLit(GDouble(sensorValue(6).toDouble)))
      }
    }
  }

  /*
  // add 3D data into a buffer
  def getThreeDData (st: Store) = {
    if (ui.App.ui.threeDtab.isInstanceOf[ui.threeD.ThreeDTab]){
      val threedtab = ui.App.ui.threeDtab.asInstanceOf[ui.threeD.ThreeDTab]
      threedtab.appModel.threeDData.getData(repr(st))
      //println("Add data to _3DData")
      threedtab.playinRealTime()
      //println("Show in real time")
    }
  }
  */

  def step(p: Prog, st: Store): Option[Store] = {
    val res = localStep(p, st)
    if (res == null) None
    else Some(st)
  }

  // always returns the last known step, the adder callback is used to
  // determine when the simulation is done
  override def multiStep(p: Prog, st: Store, adder: DataAdder): Store = {
    val magic = getSimulator(st)
    var shouldAddData = ShouldAddData.IfLast
    // ^^ set to IfLast on purpose to make things work
    @tailrec def step0() : Unit = {
      val res = localStep(p, st)
      stepInit
      if (res == null) {
        if (shouldAddData == ShouldAddData.IfLast)
          addData(st, adder)
        adder.noMoreData()
      } else {
        shouldAddData = adder.newStep(res)
        if (shouldAddData == ShouldAddData.Yes)
          addData(st, adder)
        if (adder.continue)
          step0()
      }
    }
    step0()
    getThreeDData(st)
    st
  }


  def addData(st: Store, adder: DataAdder) : Unit = {
    // Note: Conversion to a CStore just to add the data is certainly
    // not the most efficient way to go about things, but for now it
    // will do. --kevina
    adder.addData(st.id, st.fields)
    st.children.foreach { child => addData(child, adder) }
  }


  def stepInit : Unit = {}
  def traverse(f: ObjId => Changeset, root: ObjId): Changeset =
    traverseSimple(f, root)

}

// The ImperativeInterpreter is stateless hence it is okay to have a
// single global instance
object ImperativeInterpreter extends ImperativeInterpreter