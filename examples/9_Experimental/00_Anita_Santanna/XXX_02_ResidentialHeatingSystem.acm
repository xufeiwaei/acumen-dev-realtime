// Smart residential heating system 
//
// Author:  Anita Sant'Anna, 2013
//
// Run this example, view Plot, change weather profile and view plot. 
//
// This example models a smart residential heating system which
// opperates dependent on weather and electricity costs (demand response program)

// The heating system is composed of an electric heat pump, a hot water storage
// and a forced convection system (fans).

// The thermodynamic mode of the system is:
//
//   T_H'   = (K_H*(T_O - T_H) - Y_TM_H*(T_H - T_TM)) / (m_H*C_H);
//   T_TM'  = (Q_HP_prime - Y_TM_H*(T_TM - T_H)) / (m_TM*C_TM);
// 
//   where:
//     T_H: house temperature
//     T_TM: boiler temperature
//     T_O: outside temperature (according to Portland, Colorado Springs or Halmstad weather)
//     K_H: heat transfer coefficient of the house insulation
//     Y_TM_H: coefficient of heat transfer between boiler and house (due to fans)
//     m_TM: mass of the water in the boiler
//     m_H: mass of the air in the house
//     Q_HP_prime: amount of heat the heat pump transfers to the boiler

// The heat pump heats the water in the storage tank. The forced convection
// system uses air fans to tranfer heat from the water storage tank to the 
// house. 

// The heat pump operates in three different modes: low, medium and high.
// It tries to maintain the temperature of the water in the storage at 65 C.
// It must also switch off during peak time-of-use (TUR) rates when the electricity
// is the most expensive. The pump must wait 5 min between switching off and on again.
// The heat pump control follows the following rules:
//
//   Q_HP_prime = 0        if (T_TM > 65 && count > 5) or (rate > 13 && count > 5) 
//   Q_HP_prime = 10.5*0.3 if (T_TM > 64 && T_TM < 65)   
//   Q_HP_prime = 10.5*0.6 if (T_TM > 62 && T_TM < 64)
//   Q_HP_prime = 10.5     if (T_TM < 62)
//
//   where:
//     Q_HP_prime: heat transfered from heat pump to boiler
//     T_TM: temperature of water in the storage tank
//     count: time since last switch off
//     rate: time-of-use rate
//
// The total cost of electricity depends on time-of-use rates (TUR)
// and the coefficient of performance (COP) of the heat pump
// according to the following equation:
//
//  if Q_HP_prime = low    COP = -0.0002592*T_O^2 + 0.0358*T_O + 4.0181;
//  if Q_HP_prime = medium COP = -0.0002592*T_O^2 + 0.0313*T_O + 3.6381;
//  if Q_HP_prime = high   COP = -0.0002592*T_O^2 + 0.0268*T_O + 3.2581;
//
//  power=power + (Q_HP_prime/COP)*(1/60);          // [kW.h]
//  cost=cost + (rate/100)*(Q_HP_prime/COP)*(1/60); // [dollars]
// 
//  where: 
//    power: total power consumed by heat pump
//    cost: total cost of electricity used
//    rate: time-of-use rate
//    T_O: outdoor temperature
//
// Time-of-use rates are defined as follows:
//    rate = 13.266 from 06:00 to 10:00 and from 17:00 to 20:00 
//    rate = 7.500  from 10:== to 17:00 and from 20:00 to 22:00 
//    rate = 4.422  from 22:00 to 06:00 
//
// The forced convection system tries to maintain the temperature of
// the house around 21 C. It follows the follwoing rules:
//
//   Y_TM_H = 0       if (T_H >= 21)
//   Y_TM_H = 0.1303  if (T_H > 20 && T_H < 21) 
//   Y_TM_H = 0.2605  if (T_H < 20)
//   
//   where:
//     Y_TM_H: coefficient of heat transfer between boiler and house
//     T_H: temperature of the house 
//     
//
// weather profile for Portland Oregon, hourly averages for January 
class WeatherPortland()
 private mode := "hour"; T_O := 3; tx  := 1; tx' := 1/3600; i:=1; end
 tx' = 1/3600;

 switch mode
   case "hour"
     if(tx>=1)
       switch i
        case  1 T_O = 3;
        case  2 T_O = 3;
        case  3 T_O = 3;
        case  4 T_O = 3;
        case  5 T_O = 3;
        case  6 T_O = 3;
        case  7 T_O = 3;
        case  8 T_O = 3;
        case  9 T_O = 3;
        case 10 T_O = 4;
        case 11 T_O = 5;
        case 12 T_O = 5;
        case 13 T_O = 6;
        case 14 T_O = 6;
        case 15 T_O = 6;
        case 16 T_O = 5;
        case 17 T_O = 5;
        case 18 T_O = 4;
        case 19 T_O = 4;
        case 20 T_O = 4;
        case 21 T_O = 4;
        case 22 T_O = 4;
        case 23 T_O = 4;
        case 24 T_O = 3;        
       end;
      i := i + 1;
      tx:=0;
      if(i>24) mode := "NoMore" end
     end
  case "NoMore"
  end
end

// weather profile for Halmstad, Sweden, hourly averages for January 
class WeatherHalmstad()
 private mode := "hour"; T_O := 0; tx  := 1; tx' := 1/3600; i:=1; end
 tx' = 1/3600;
 switch mode
   case "hour"
     if(tx>=1)
       switch i
        case  1 T_O = 0;
        case  2 T_O = 0;
        case  3 T_O = 0;
        case  4 T_O = 0;
        case  5 T_O = 0;
        case  6 T_O = 0;
        case  7 T_O = 0;
        case  8 T_O = 0;
        case  9 T_O = 0;
        case 10 T_O = 0;
        case 11 T_O = 1;
        case 12 T_O = 1;
        case 13 T_O = 1;
        case 14 T_O = 1;
        case 15 T_O = 1;
        case 16 T_O = 1;
        case 17 T_O = 1;
        case 18 T_O = 1;
        case 19 T_O = 0;
        case 20 T_O = 0;
        case 21 T_O = 0;
        case 22 T_O = 0;
        case 23 T_O = 0;
        case 24 T_O = 0;        
       end;
      i := i + 1;
      tx:=0;
      if(i>24) mode := "NoMore" end
     end
  case "NoMore"
  end
end

// weather profile for Colorado Springs, CO, hourly averages for January 
class WeatherColoradoSprings()
 private mode := "hour"; T_O := -5; tx  := 1; tx' := 1/3600; i:=1; end
 tx' = 1/3600;
 switch mode
   case "hour"
     if(tx>=1)
       switch i
        case  1 T_O = -5;
        case  2 T_O = -5;
        case  3 T_O = -5;
        case  4 T_O = -5;
        case  5 T_O = -5;
        case  6 T_O = -5;
        case  7 T_O = -5;
        case  8 T_O = -4;
        case  9 T_O = -2;
        case 10 T_O = 1;
        case 11 T_O = 2;
        case 12 T_O = 3;
        case 13 T_O = 3;
        case 14 T_O = 4;
        case 15 T_O = 3;
        case 16 T_O = 2;
        case 17 T_O = 0;
        case 18 T_O = -2;
        case 19 T_O = -3;
        case 20 T_O = -3;
        case 21 T_O = -4;
        case 22 T_O = -4;
        case 23 T_O = -5;
        case 24 T_O = -5        
       end;
      i := i + 1;
      tx:=0;
      if(i>24) mode := "NoMore" end
     end
  case "NoMore"
  end
end


class CostAssessment()
  private Q_HP_prime:=0; COP:=0; rate:=0; power:=0; cost:=0; mode:="off"; T_O:=0; end
  switch mode
    case "off"
    COP=0;
    power=power;    // [kW.h]
    cost=cost;      // [cents]
    if (Q_HP_prime > 1 && Q_HP_prime < 3) mode:="low"; 
    else 
      if (Q_HP_prime > 4 && Q_HP_prime < 5) mode:="medium"; 
      else
        if (Q_HP_prime > 6 && Q_HP_prime < 11) mode:="high";  end
      end
    end

    case "low"
    COP=-0.0002592*T_O^2 + 0.0358*T_O + 4.0181;
    power=power + (Q_HP_prime/COP)*(1/60);          // [kW.h]
    cost=cost + (rate/100)*(Q_HP_prime/COP)*(1/60); // [dollars]
    if (Q_HP_prime > 1 && Q_HP_prime < 3) mode:="low"; 
    else 
      if (Q_HP_prime > 4 && Q_HP_prime < 5) mode:="medium"; 
      else
        if (Q_HP_prime > 6 && Q_HP_prime < 11) mode:="high";  end
      end
    end

    case "medium"
    COP=-0.0002592*T_O^2 + 0.0313*T_O + 3.6381;
    power=power + (Q_HP_prime/COP)*(1/60);          // [kW.h]
    cost=cost + (rate/100)*(Q_HP_prime/COP)*(1/60); // [dollars]
    if (Q_HP_prime > 1) mode:="on"; end

    case "high"
    COP=-0.0002592*T_O^2 + 0.0268*T_O + 3.2581;
    power=power + (Q_HP_prime/COP)*(1/60);          // [kW.h]
    cost=cost + (rate/100)*(Q_HP_prime/COP)*(1/60); // [dollars]
    if (Q_HP_prime > 1 && Q_HP_prime < 3) mode:="low"; 
    else 
      if (Q_HP_prime > 4 && Q_HP_prime < 5) mode:="medium"; 
      else
        if (Q_HP_prime > 6 && Q_HP_prime < 11) mode:="high"; end 
      end
    end

  end 
end

// Time of use rates determine the cost of electricity depending on the hour of the day
class TimeUseRate()
  private rate:=4.422; t:=0; t':=1/3600; mode:="off_peak" end
  t'=1/3600; //time in hours  
  switch mode
    case "off_peak"
    rate = 4.422;
    if (t > 6 && t < 10) mode:="on_peak"; end
    
    case "mid_peak"
    rate = 7.5;
    if (t > 17 && t < 20) mode:="on_peak";
    else
      if (t > 22) || (t < 6) mode:="off_peak"; end
    end

    case "on_peak"
    rate = 13.266;
    if (t > 10 && t < 17) || (t > 20 && t < 22) 
      mode:="mid_peak"; 
    end
  end
end



class HeatPump()
  private Q_HP_prime:=0; T_TM:=65; mode:="off"; rate:=0; count:=0; end
  count=count+1;
  switch mode
    case "high"
    Q_HP_prime = 10.5;
    if (T_TM > 65 && count > 5) || (rate > 13 && count > 5) 
      mode:="off"; count:=0; 
    else
      if (T_TM > 64 && T_TM < 65) mode:="low"; 
      else 
        if (T_TM > 62 && T_TM < 64) mode:="medium"; end
      end
    end

    case "medium"
    Q_HP_prime = 10.5*0.6;
     if (T_TM > 65 && count > 5) || (rate > 13 && count > 5) 
      mode:="off"; count:=0; 
    else
      if (T_TM > 64 && T_TM < 65) mode:="low"; 
      else 
        if (T_TM < 62) mode:="high"; end;
      end
    end

    case "low"
    Q_HP_prime = 10.5*0.3;
    if (T_TM > 66 && count > 5) || (rate > 13 && count > 5) 
      mode:="off"; count:=0; 
    else
      if (T_TM > 62 && T_TM < 64) mode:="medium";  
      else
        if (T_TM < 62) mode:="high"; end 
      end
    end

    case "off"
    Q_HP_prime = 0;
    if (T_TM > 64 && T_TM < 65 && count>5 && rate < 13) mode:="low"; 
    else 
      if (T_TM > 62 && T_TM < 64 && count>5 && rate < 13) mode:="medium";  
      else
        if (T_TM < 62 && count>5 && rate < 13) mode:="high"; end 
      end
    end
    
  end
end

class HeatingSystem(m_H, K_H, C_H, m_TM, C_TM)
  private
    Y_TM_H:=0; T_H:=20; T_H':=0; T_TM:=65; T_TM':=0; T_O:=0; Q_HP_prime:=0;
    mode:="fan_off";
  end
  T_H'   = (K_H*(T_O - T_H) - Y_TM_H*(T_H - T_TM)) / (m_H*C_H);
  T_TM'  = (Q_HP_prime - Y_TM_H*(T_TM - T_H)) / (m_TM*C_TM);

  switch mode
    case "fan_off"
    Y_TM_H=0;
    if (T_H > 20 && T_H < 21) mode:="fan_low"; 
    else
      if(T_H < 20) mode:="fan_high"; end 
    end

    case "fan_high"
    Y_TM_H=0.2605;
    if (T_H > 20 && T_H < 21) mode:="fan_low"; 
    else 
      if (T_H >= 21) mode:="fan_off"; end
    end

    case "fan_low"
    Y_TM_H=0.1303;
    if (T_H < 20) mode:="fan_high"; 
    else
       if (T_H >= 21) mode:="fan_off"; end
   end

  end
end

class Main(simulator)
  private
    pump   := create HeatPump();
    system := create HeatingSystem(913.218,0.1474,1.0050,624.525,4.136);
    TUR := create TimeUseRate();   
    // note you may change the weather profile to Portland or Colorado Springs
    weather := create WeatherHalmstad();
    assessment := create CostAssessment();
    t:=0; t':=1/3600;
  end

  t'=1/3600;
  // linking variables from different classes
  system.Q_HP_prime = pump.Q_HP_prime;
  pump.T_TM = system.T_TM;
  pump.rate = TUR.rate;
  system.T_O = weather.T_O;
  assessment.rate=TUR.rate;
  assessment.Q_HP_prime=pump.Q_HP_prime;
  assessment.T_O=weather.T_O;
  // simulation parameters
  simulator.endTime =86400;
  simulator.timeStep=60;
end